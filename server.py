from flask import Flask, request, make_response,redirect,render_template #clase Flask de extensión flask
from Crud import Crud
import json
app = Flask(__name__) #instancia de Flask

"""
#forma de crear rutas
@app.route('/')#ruta raiz
#lo que se va a hacer cuando se acceda a esa ruta
#Lo que se va a hacer se define a través de una funcion
def hello():
    ip = request.remote_addr
    retorno = "su ip es: "+str(ip)
    #return "Hola mundo"
    return retorno #sera mostrado en el navegador
"""
"""
@app.route('/')
def index():
    user_ip = request.remote_addr
    response=make_response(redirect('/get_bus'))
    response.set_cookie('user_ip',user_ip)
    return response
"""
@app.route('/')
def index():
    user_ip = request.remote_addr
    variable2="Variable2"
    cr = Crud("containers-us-west-51.railway.app",
              "railway",
              "postgres",
              "qQH63P3mwGAO4Rmy1Nto")
    buses = cr.leer_buses() 
    buses = cr.leer_buses() #buses es ua lista de tuplas, cada registro es una tupla   
    lista_buses=[]
    for bus in buses: #bus es la tupla de cada registro
        lista_buses.append( {"id": bus[0],
                           "modelo": bus[1],
                           "capacidad": bus[2],
                           "placa": bus[3],
                           "fabricante": bus[4]} )
    respuesta = json.dumps(lista_buses) # convertir lista en formato JSON
    cr.close()#cerrando conexion 
    return render_template("index.html",ip=user_ip,v2=variable2,buses=lista_buses)

@app.route('/show_edit_bus_form', methods=['GET','POST'])
def show_edit_bus_form():
    if(request.method=='POST'):
        id_bus=request.form.get("id_bus")
        print("El id de bus es: ",id_bus)
        cr = Crud("containers-us-west-51.railway.app",
              "railway",
              "postgres",
              "qQH63P3mwGAO4Rmy1Nto")
        bus = cr.leer_bus(id_bus) #buses es ua lista de tuplas, cada registro es una tupla
        bus = bus[0]
        print(bus)
        dic_bus = {"id":bus[0],"modelo":bus[1],"capacidad":bus[2],"placa":bus[3],"marca":bus[4]}
        return render_template('edit_bus.html',bus=dic_bus)

@app.route('/edit_bus',methods=['GET','POST'])
def edit_bus():
    if(request.method=='POST'):
        id=request.form.get("id")
        modelo=request.form.get("modelo")
        capacidad=request.form.get("capacidad")
        placa=request.form.get("placa")
        marca=request.form.get("marca")
        print("id=",id, "modelo=",modelo, " capacidad=",capacidad, " placa=",placa, "marca=",marca)
        cr = Crud("containers-us-west-51.railway.app",
              "railway",
              "postgres",
              "qQH63P3mwGAO4Rmy1Nto")
        cr.update_bus(id,modelo,capacidad,placa,marca) #buses es ua lista de tuplas, cada registro es una tupla
        response=make_response(redirect('/get_buses'))
        return response


@app.route("/delete_bus",methods=['GET','POST'])
def delete_bus():
    if(request.method=='POST'):
        id=request.form.get("id_bus")
        cr = Crud("containers-us-west-51.railway.app",
              "railway",
              "postgres",
              "qQH63P3mwGAO4Rmy1Nto")
        cr.eliminar_bus(id)
        cr.close
        response=make_response(redirect('/get_buses'))
        return response

    

@app.route('/login', methods=['GET','POST'])
def login():
    if(request.method=='GET'):
        return render_template('login.html')
    if(request.method=='POST'):
        nombreUsuario = request.form.get("username")
        passswd = request.form.get("passwd")
        print("Nombre de usuario= "+nombreUsuario+" passwd="+passswd)
        return render_template('login.html')
    

@app.route('/form_add_bus')
def form_add_bus():
    return render_template('add_bus.html')

@app.route("/add_bus", methods=['GET','POST'])  
def add_bus():
     if(request.method=='POST'):
        modelo=request.form.get("modelo")
        capacidad=request.form.get("capacidad")
        placa=request.form.get("placa")
        marca=request.form.get("marca")
        print("id=",id, "modelo=",modelo, " capacidad=",capacidad, " placa=",placa, "marca=",marca)
        cr = Crud("containers-us-west-51.railway.app",
              "railway",
              "postgres",
              "qQH63P3mwGAO4Rmy1Nto")
        cr.insertar_bus(modelo,capacidad,placa,marca) #buses es ua lista de tuplas, cada registro es una tupla
        cr.close()
        response=make_response(redirect('/get_buses'))
        return response




@app.route('/hello')
def hello():
    user_ip=request.cookies.get('user_ip')
    return "La ip del PC es: "+str(user_ip)

@app.route('/division')
def division():
    div = 10/5
    return "La division es: "+str(div)

@app.route('/multiplicar')
def multiplicar():
    mul = 10*5 
    return "La multiplicacion es: "+str(mul)

@app.route('/ejercicio')
def ejercicio():
    numero=int(input("Ingrese un numero "))
    return "El cuadrado del numero es "+str(numero**2)

@app.route('/get_bus')
def get_bus():
    cr = Crud("containers-us-west-51.railway.app",
              "railway",
              "postgres",
              "qQH63P3mwGAO4Rmy1Nto")
    buses = cr.leer_buses() #buses es ua lista de tuplas, cada registro es una tupla
    primer_bus=buses[0] #tomo  el primer registro (la primera tupla)
    print(primer_bus)
    #se declara un diccionario en formato JSON, con dumps se convierte a formato JSON
    respuesta = json.dumps( {"id": primer_bus[0],
                           "modelo": primer_bus[1],
                           "capacidad": primer_bus[2],
                           "placa": primer_bus[3],
                           "fabricante": primer_bus[4]} )
    cr.close()#cerrando conexion 
    #Convertir los datos de esa tupla a un string
    #primer_bus_string = "id: "+str(primer_bus[0])+" modelo: "+str(primer_bus[1])+\
    #" capacidad: "+str(primer_bus[2])+" placa: "+primer_bus[3] + " fabricante: "+primer_bus[4]
    #return primer_bus_string
    return respuesta

@app.route('/get_buses')
def get_buses():
    cr = Crud("containers-us-west-51.railway.app",
              "railway",
              "postgres",
              "qQH63P3mwGAO4Rmy1Nto")
    buses = cr.leer_buses() #buses es ua lista de tuplas, cada registro es una tupla   
    lista_buses=[]
    for bus in buses: #bus es la tupla de cada registro
        lista_buses.append( {"id": bus[0],
                           "modelo": bus[1],
                           "capacidad": bus[2],
                           "placa": bus[3],
                           "fabricante": bus[4]} )
    #respuesta = json.dumps(lista_buses) # convertir lista en formato JSON
    #return respuesta
    #respuesta = json.dumps(lista_buses) # convertir lista en formato JSON
    return render_template("mostrar_buses.html",buses=lista_buses)


if __name__=="__main__":
    #punto de partida de ejecucion del programa
    
    print("Arrancando el servidor...")
    app.run(debug=True,host='0.0.0.0')

"""
def hello():
    suma =5+5
    print("Mostrando suma desde la terminal - suma es: ",suma)
    return "Hola mundo" #esto sera mostrado en el navegador

if __name__=="__main__":
    #punto de partida de ejecucion del programa
    print("Arrancando el servidor...")
    app.run(debug=False,host='0.0.0.0')

"""




"""
#primer ruta
@app.route('/')#ruta en el navegador
def hello():
    return 'Hello world from Flask'

if __name__=="__main__":
    while(True):
        print("Starting web server")
        app.run(debug=False,host='0.0.0.0')
    
"""